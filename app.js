// Setup our dependencies

const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");


// Server setup

const app = express();
const port = 2001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// MongoDB Connection
mongoose.connect("mongodb+srv://joram_182:joramape182@zuitt-bootcamp.kq3szvv.mongodb.net/s36-todo?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

mongoose.connection.once("open", () => console.log('Now connected to the database!'));

// add the task route
app.use("/tasks", taskRoute); // By writing this, all the task routes would start with /tasks


// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));


